﻿using System.Collections.Concurrent;

namespace BlubLib.Collections.Concurrent
{
    public static class ConcurrentDictionaryExtensions
    {
        public static bool Remove<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> @this, TKey key)
        {
            return @this.TryRemove(key, out var _);
        }

        // Fix ambiguous reference problem
        // ConcurrentDictionary implements IDictionary and IReadOnlyDictionary
        public static TValue GetValueOrDefault<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> @this, TKey key)
        {
            return @this.TryGetValue(key, out var value) ? value : default(TValue);
        }
    }
}
