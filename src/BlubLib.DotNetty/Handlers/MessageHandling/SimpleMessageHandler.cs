﻿using System;
using System.Collections.Concurrent;
using BlubLib.Collections.Concurrent;
using DotNetty.Common.Utilities;
using DotNetty.Transport.Channels;

namespace BlubLib.DotNetty.Handlers.MessageHandling
{
    public class SimpleMessageHandler : ChannelHandlerAdapter
    {
        private readonly ConcurrentDictionary<Type, IMessageHandler> _handlers =
            new ConcurrentDictionary<Type, IMessageHandler>();

        public event EventHandler<MessageEventArgs> MessageHandled;
        public event EventHandler<MessageEventArgs> MessageUnhandled;

        protected virtual void OnHandledMessage(object message)
        {
            MessageHandled?.Invoke(this, new MessageEventArgs(message));
        }

        protected virtual void OnUnhandledMessage(object message)
        {
            MessageUnhandled?.Invoke(this, new MessageEventArgs(message));
        }

        public override async void ChannelRead(IChannelHandlerContext context, object message)
        {
            var release = true;
            try
            {
                var handled = false;
                foreach (var entry in _handlers.Values)
                {
                    if (await entry.OnMessageReceived(context, message))
                        handled = true;
                }

                if (handled)
                {
                    OnHandledMessage(message);
                }
                else
                {
                    release = false;
                    OnUnhandledMessage(message);
                    base.ChannelRead(context, message);
                }
            }
            catch (Exception ex)
            {
                context.Channel.Pipeline.FireExceptionCaught(ex);
            }
            finally
            {
                if (release)
                    ReferenceCountUtil.Release(message);
            }
        }

        public SimpleMessageHandler Add(IMessageHandler handler)
        {
            if (!_handlers.TryAdd(handler.GetType(), handler))
                throw new ArgumentException("Type already exists");

            return this;
        }

        public T Get<T>()
            where T : IMessageHandler
        {
            _handlers.TryGetValue(typeof(T), out var service);
            return (T)service;
        }

        public void Remove<T>()
            where T : IMessageHandler
        {
            _handlers.Remove(typeof(T));
        }
    }
}
