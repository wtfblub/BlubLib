﻿using DotNetty.Common.Utilities;
using ResponseQueue = System.Collections.Concurrent.ConcurrentDictionary<System.Guid, System.Threading.Tasks.TaskCompletionSource<BlubLib.DotNetty.SimpleRmi.RmiMessage>>;

namespace BlubLib.DotNetty.SimpleRmi
{
    internal static class ChannelAttributes
    {
        public static readonly AttributeKey<ResponseQueue> ResponseQueue =
            AttributeKey<ResponseQueue>.ValueOf($"BlubLib.DotNetty.SimpleRmi-{nameof(ResponseQueue)}");
    }
}
