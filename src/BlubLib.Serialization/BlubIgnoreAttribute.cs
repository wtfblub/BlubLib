using System;

namespace BlubLib.Serialization
{
    /// <summary>
    /// Tells the <see cref="BlubSerializer"/> to not include the marked member
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field,
        Inherited = false, AllowMultiple = false)]
    public class BlubIgnoreAttribute : Attribute
    {
    }
}
