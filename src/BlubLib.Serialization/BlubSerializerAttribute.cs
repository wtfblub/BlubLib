using System;

namespace BlubLib.Serialization
{
    /// <summary>
    /// Tells the <see cref="T:BlubLib.Serialization.BlubSerializer" /> which serializer should be used
    /// </summary>
    [AttributeUsage(
        AttributeTargets.Class | AttributeTargets.Struct |
        AttributeTargets.Property | AttributeTargets.Field,
        Inherited = false, AllowMultiple = false)]
    public class BlubSerializerAttribute : Attribute
    {
        public Type SerializerType { get; set; }
        public object[] SerializerParameters { get; set; }

        public BlubSerializerAttribute(Type serializerType, params object[] serializerParameters)
        {
            if (serializerType == null)
                throw new ArgumentNullException(nameof(serializerType));

            SerializerType = serializerType;
            SerializerParameters = serializerParameters;
        }
    }
}
