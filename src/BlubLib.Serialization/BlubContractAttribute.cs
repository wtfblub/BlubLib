﻿using System;

namespace BlubLib.Serialization
{
    /// <summary>
    /// Tells the <see cref="BlubSerializer"/> to only include members marked with <see cref="BlubMemberAttribute"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct,
        Inherited = false, AllowMultiple = false)]
    public class BlubContractAttribute : Attribute
    {
    }
}
