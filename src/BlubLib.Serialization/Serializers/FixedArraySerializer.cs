﻿using System;
using System.IO;
using BlubLib.Reflection;
using Sigil;

namespace BlubLib.Serialization.Serializers
{
    /// <summary>
    /// Serializer for fixed size arrays
    /// </summary>
    /// <remarks>The serializer will use <see cref="BinaryWriter.Write(byte[])"/>/<see cref="BinaryReader.ReadBytes"/> for byte arrays and for other types it looks for an appropriate serializer</remarks>
    public class FixedArraySerializer : ISerializerCompiler
    {
        private readonly int _length;

        /// <summary>
        /// Initializes a new instance of the <see cref="FixedArraySerializer"/> class.
        /// </summary>
        /// <param name="length">The length of the array. Must be 0 or higher</param>
        public FixedArraySerializer(int length)
        {
            if (length < 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            _length = length;
        }

        /// <inheritdoc/>
        public bool CanHandle(Type type)
        {
            return type.IsArray;
        }

        /// <inheritdoc/>
        public void EmitDeserialize(CompilerContext ctx, Local value)
        {
            var elementType = value.LocalType.GetElementType();

            if (_length <= 0)
            {
                ctx.Emit.Call(typeof(Array)
                    .GetMethod(nameof(Array.Empty))
                    .GetGenericMethodDefinition()
                    .MakeGenericMethod(elementType));
                ctx.Emit.StoreLocal(value);
            }
            else
            {
                // Little optimization for byte arrays
                if (elementType == typeof(byte))
                {
                    // value = reader.ReadBytes(length);
                    ctx.Emit.LoadReaderOrWriterParam();
                    ctx.Emit.LoadConstant(_length);
                    ctx.Emit.CallVirtual(ReflectionHelper.GetMethod((BinaryReader _) => _.ReadBytes(default(int))));
                    ctx.Emit.StoreLocal(value);
                }
                else
                {
                    var loop = ctx.Emit.DefineLabel();
                    var loopCheck = ctx.Emit.DefineLabel();

                    // value = new [length]
                    ctx.Emit.LoadConstant(_length);
                    ctx.Emit.NewArray(elementType);
                    ctx.Emit.StoreLocal(value);

                    using (var element = ctx.Emit.DeclareLocal(elementType, "element"))
                    using (var i = ctx.Emit.DeclareLocal<int>("i"))
                    {
                        ctx.Emit.MarkLabel(loop);
                        ctx.EmitDeserialize(element);

                        // value[i] = element
                        ctx.Emit.LoadLocal(value);
                        ctx.Emit.LoadLocal(i);
                        ctx.Emit.LoadLocal(element);
                        ctx.Emit.StoreElement(elementType);

                        // ++i
                        ctx.Emit.LoadLocal(i);
                        ctx.Emit.LoadConstant(1);
                        ctx.Emit.Add();
                        ctx.Emit.StoreLocal(i);

                        // i < length
                        ctx.Emit.MarkLabel(loopCheck);
                        ctx.Emit.LoadLocal(i);
                        ctx.Emit.LoadConstant(_length);
                        ctx.Emit.BranchIfLess(loop);
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void EmitSerialize(CompilerContext ctx, Local value)
        {
            var elementType = value.LocalType.GetElementType();
            if (_length <= 0)
                return;

            // Little optimization for byte arrays
            if (elementType == typeof(byte))
            {
                // writer.WriteBytes(value);
                ctx.Emit.LoadReaderOrWriterParam();
                ctx.Emit.LoadLocal(value);
                ctx.Emit.CallVirtual(ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(byte[]))));
            }
            else
            {
                var loop = ctx.Emit.DefineLabel();
                var loopCheck = ctx.Emit.DefineLabel();
                using (var element = ctx.Emit.DeclareLocal(elementType, "element"))
                using (var i = ctx.Emit.DeclareLocal<int>("i"))
                {
                    ctx.Emit.Branch(loopCheck);
                    ctx.Emit.MarkLabel(loop);

                    // element = value[i]
                    ctx.Emit.LoadLocal(value);
                    ctx.Emit.LoadLocal(i);
                    ctx.Emit.LoadElement(elementType);
                    ctx.Emit.StoreLocal(element);

                    ctx.EmitSerialize(element);

                    // ++i
                    ctx.Emit.LoadLocal(i);
                    ctx.Emit.LoadConstant(1);
                    ctx.Emit.Add();
                    ctx.Emit.StoreLocal(i);

                    // i < length
                    ctx.Emit.MarkLabel(loopCheck);
                    ctx.Emit.LoadLocal(i);
                    ctx.Emit.LoadConstant(_length);
                    ctx.Emit.BranchIfLess(loop);
                }
            }
        }
    }
}
