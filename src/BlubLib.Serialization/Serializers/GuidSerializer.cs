﻿using System;
using System.IO;
using BlubLib.Reflection;
using Sigil;

namespace BlubLib.Serialization.Serializers
{
    /// <summary>
    /// Serializer for <see cref="Guid"/>
    /// </summary>
    public class GuidSerializer : ISerializerCompiler
    {
        /// <inheritdoc/>
        public bool CanHandle(Type type)
        {
            return type == typeof(Guid);
        }

        /// <inheritdoc/>
        public void EmitDeserialize(CompilerContext ctx, Local value)
        {
            // value = new Guid(reader.ReadBytes(16))
            ctx.Emit.LoadReaderOrWriterParam();
            ctx.Emit.LoadConstant(16);
            ctx.Emit.CallVirtual(ReflectionHelper.GetMethod((BinaryReader _) => _.ReadBytes(default(int))));
            ctx.Emit.NewObject<Guid, byte[]>();
            ctx.Emit.StoreLocal(value);
        }

        /// <inheritdoc/>
        public void EmitSerialize(CompilerContext ctx, Local value)
        {
            // writer.Write(value.ToByteArray())
            ctx.Emit.LoadReaderOrWriterParam();
            ctx.Emit.LoadLocalAddress(value);
            ctx.Emit.Call(ReflectionHelper.GetMethod((Guid _) => _.ToByteArray()));
            ctx.Emit.CallVirtual(ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(byte[]))));
        }
    }
}
