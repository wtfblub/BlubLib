using System;
using System.IO;

namespace BlubLib.Serialization
{
    /// <summary>
    /// Implement <see cref="ISerializer{T}"/> instead
    /// This is used internally to store the serializer
    /// </summary>
    public interface ISerializer
    {
        /// <summary>
        /// Gets an indication whether the serializer can handle the type
        /// </summary>
        /// <param name="type">The type to handle</param>
        /// <returns>True if the serializer can handle the type</returns>
        bool CanHandle(Type type);
    }

    /// <summary>
    /// Provides methods for serialization
    /// </summary>
    /// <typeparam name="T">Object/Value type this serializer handles</typeparam>
    public interface ISerializer<T> : ISerializer
    {
        /// <summary>
        /// Serializes an object or value
        /// </summary>
        /// <param name="blubSerializer">The current BlubSerializer</param>
        /// <param name="writer">The writer containing the serialized data</param>
        /// <param name="value">The object or value to serialize</param>
        void Serialize(BlubSerializer blubSerializer, BinaryWriter writer, T value);

        /// <summary>
        /// Deserializes an object or value
        /// </summary>
        /// <param name="blubSerializer">The current BlubSerializer</param>
        /// <param name="reader">The reader containing the serialized data</param>
        /// <returns>The deserialized object or value</returns>
        T Deserialize(BlubSerializer blubSerializer, BinaryReader reader);
    }
}
