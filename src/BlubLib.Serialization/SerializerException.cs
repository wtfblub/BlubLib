using System;

namespace BlubLib.Serialization
{
    public class SerializerException : Exception
    {
        public SerializerException(string message)
            : base(message)
        {
        }
    }
}
