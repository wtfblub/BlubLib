﻿using System;
using System.IO;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Diagnosers;
using BenchmarkDotNet.Running;
using BlubLib.IO;
using BlubLib.Serialization.Serializers;

namespace BlubLib.Serialization.Benchmark
{
    internal static class Program
    {
        private static void Main()
        {
            var config = ManualConfig
                .Create(DefaultConfig.Instance)
                .With(MemoryDiagnoser.Default);
            BenchmarkRunner.Run<Benchmark>(config);
        }
    }

    public class Benchmark
    {
        private readonly BlubSerializer _serializer = new BlubSerializer();
        private readonly MemoryStream _stream;
        private readonly MemoryStream _readerStream;
        private readonly BinaryWriter _writer;
        private readonly BinaryReader _reader;
        private readonly Model _model;
        private readonly ModelStruct _modelStruct;

        public Benchmark()
        {
            _stream = new MemoryStream();
            _readerStream = new MemoryStream();
            _writer = _stream.ToBinaryWriter(false);
            _reader = _readerStream.ToBinaryReader(false);

            var rng = new Random(Guid.NewGuid().GetHashCode());
            _model = new Model
            {
                Value1 = rng.Next(),
                Value2 = Guid.NewGuid(),
                Value3 = rng.Next().ToString(),
                Value4 = new byte[1000]
            };
            rng.NextBytes(_model.Value4);

            _modelStruct = new ModelStruct
            {
                Value1 = rng.Next(),
                Value2 = Guid.NewGuid(),
                Value3 = rng.Next().ToString(),
                Value4 = new byte[1000]
            };
            rng.NextBytes(_modelStruct.Value4);

            using (var w = _readerStream.ToBinaryWriter(true))
                _serializer.Serialize(w, _model);
        }

        [Benchmark]
        public void Serialize()
        {
            _stream.Position = 0;
            _serializer.Serialize(_writer, _model);
        }

        [Benchmark]
        public void SerializeStruct()
        {
            _stream.Position = 0;
            _serializer.Serialize(_writer, _modelStruct);
        }

        [Benchmark]
        public Model Deserialize()
        {
            _readerStream.Position = 0;
            return _serializer.Deserialize<Model>(_reader);
        }

        [Benchmark]
        public ModelStruct DeserializeStruct()
        {
            _readerStream.Position = 0;
            return _serializer.Deserialize<ModelStruct>(_reader);
        }
    }

    [BlubContract]
    public class BaseModel
    {
        [BlubMember(0)]
        public int Value1 { get; set; }

        [BlubMember(1)]
        public Guid Value2 { get; set; }
    }

    public class Model : BaseModel
    {
        public string Value3 { get; set; }

        [BlubSerializer(typeof(FixedArraySerializer), 1000)]
        public byte[] Value4 { get; set; }
    }

    [BlubContract]
    public struct ModelStruct
    {
        public int Value1 { get; set; }

        public Guid Value2 { get; set; }

        public string Value3 { get; set; }

        [BlubSerializer(typeof(FixedArraySerializer), 1000)]
        public byte[] Value4 { get; set; }
    }
}
