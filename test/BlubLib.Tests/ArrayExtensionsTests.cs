﻿using System.Collections.Generic;
using System.Linq;
using BlubLib.Collections.Generic;
using BlubLib.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BlubLib.Tests
{
    [TestClass]
    public class ArrayExtensionsTests
    {
        [TestMethod]
        public void ForEach()
        {
            var arr = Enumerable.Range(1, 100);
            var objs = new List<int>();

            arr.ForEach(x => objs.Add(x));

            CollectionAssert.AreEqual(arr.ToArray(), objs);
        }

        [TestMethod]
        public void ForEachWithIndex()
        {
            var arr = Enumerable.Range(1, 100);
            var objs = new List<int>();
            var indices = new List<int>();

            arr.ForEach((i, x) =>
            {
                indices.Add(i);
                objs.Add(x);
            });

            CollectionAssert.AreEqual(arr.ToArray(), objs);
            CollectionAssert.AreEqual(Enumerable.Range(0, 100).ToArray(), indices);
        }

        [TestMethod]
        public void ForEachAsync()
        {
            var arr = Enumerable.Range(1, 100);
            var objs = new List<int>();

            arr.ForEachAsync(async x => objs.Add(x)).WaitEx();

            CollectionAssert.AreEqual(arr.ToArray(), objs);
        }

        [TestMethod]
        public void ForEachAsyncWithIndex()
        {
            var arr = Enumerable.Range(1, 100);
            var objs = new List<int>();
            var indices = new List<int>();

            arr.ForEachAsync(async (i, x) =>
            {
                indices.Add(i);
                objs.Add(x);
            }).WaitEx();

            CollectionAssert.AreEqual(arr.ToArray(), objs);
            CollectionAssert.AreEqual(Enumerable.Range(0, 100).ToArray(), indices);
        }
    }
}
