﻿namespace BlubLib.Serialization.Tests.Models
{
    public class WithFieldsModel
    {
        [BlubMember(0)]
        public byte A;

        [BlubMember(1)]
        public byte B { get; set; }
    }
}
