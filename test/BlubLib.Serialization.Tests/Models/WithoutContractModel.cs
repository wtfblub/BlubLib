namespace BlubLib.Serialization.Tests.Models
{
    public class WithoutContractModel
    {
        public byte A { get; set; }

        [BlubIgnore]
        public byte B { get; set; }

        public byte C { get; set; }

        [BlubIgnore]
        public byte D { get; set; }
    }
}
