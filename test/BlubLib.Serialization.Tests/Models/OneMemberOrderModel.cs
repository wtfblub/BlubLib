﻿namespace BlubLib.Serialization.Tests.Models
{
    public class OneMemberOrderModel
    {
        [BlubMember(0)]
        public byte A { get; set; }

        public byte B { get; set; }
    }
}
