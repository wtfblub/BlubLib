﻿using System;
using System.IO;
using BlubLib.Serialization.Tests.Models;
using FluentAssertions;
using Xunit;

namespace BlubLib.Serialization.Tests
{
    public class SerializersTests
    {
        [Theory]
        [InlineData((ulong)123, new byte[] { 0x7B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })]
        [InlineData((long)123, new byte[] { 0x7B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })]
        [InlineData((uint)123, new byte[] { 0x7B, 0x00, 0x00, 0x00 })]
        [InlineData(123, new byte[] { 0x7B, 0x00, 0x00, 0x00 })]
        [InlineData((ushort)123, new byte[] { 0x7B, 0x00 })]
        [InlineData((short)123, new byte[] { 0x7B, 0x00 })]
        [InlineData((byte)123, new byte[] { 0x7B })]
        [InlineData((sbyte)123, new byte[] { 0x7B })]
        [InlineData(1.23d, new byte[] { 0xAE, 0x47, 0xE1, 0x7A, 0x14, 0xAE, 0xF3, 0x3F })]
        [InlineData(1.23f, new byte[] { 0xA4, 0x70, 0x9D, 0x3F })]
        [InlineData(true, new byte[] { 0x01 })]
        [InlineData('1', new byte[] { 0x31 })]
        [InlineData("123", new byte[] { 0x03, 0x31, 0x32, 0x33 })]
        public void Serialize(object value, byte[] expected)
        {
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, value);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Theory]
        [InlineData((ulong)123, new byte[] { 0x7B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })]
        [InlineData((long)123, new byte[] { 0x7B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })]
        [InlineData((uint)123, new byte[] { 0x7B, 0x00, 0x00, 0x00 })]
        [InlineData(123, new byte[] { 0x7B, 0x00, 0x00, 0x00 })]
        [InlineData((ushort)123, new byte[] { 0x7B, 0x00 })]
        [InlineData((short)123, new byte[] { 0x7B, 0x00 })]
        [InlineData((byte)123, new byte[] { 0x7B })]
        [InlineData((sbyte)123, new byte[] { 0x7B })]
        [InlineData(1.23d, new byte[] { 0xAE, 0x47, 0xE1, 0x7A, 0x14, 0xAE, 0xF3, 0x3F })]
        [InlineData(1.23f, new byte[] { 0xA4, 0x70, 0x9D, 0x3F })]
        [InlineData(true, new byte[] { 0x01 })]
        [InlineData('1', new byte[] { 0x31 })]
        [InlineData("123", new byte[] { 0x03, 0x31, 0x32, 0x33 })]
        public void Deserialize(object expected, byte[] serialized)
        {
            var ms = new MemoryStream(serialized);
            var obj = BlubSerializer.Instance.Deserialize(ms, expected.GetType());
            obj.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void SerializeGuid()
        {
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, Guid.Empty);
            ms.ToArray().Should().BeEquivalentTo(new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });
        }

        [Fact]
        public void DeserializeGuid()
        {
            var ms = new MemoryStream(new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });
            var obj = BlubSerializer.Instance.Deserialize<Guid>(ms);
            obj.Should().Be(Guid.Empty);
        }

        [Fact]
        public void SerializeEnum()
        {
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, TestEnum.A);
            ms.ToArray().Should().BeEquivalentTo(new byte[] { 0x01, 0x00, 0x00, 0x00 });
        }

        [Fact]
        public void DeserializeEnum()
        {
            var ms = new MemoryStream(new byte[] { 0x01, 0x00, 0x00, 0x00 });
            var obj = BlubSerializer.Instance.Deserialize<TestEnum>(ms);
            obj.Should().BeEquivalentTo(TestEnum.A);
        }

        [Fact]
        public void SerializeCString()
        {
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, new CStringModel("123", "123"));
            ms.ToArray().Should().BeEquivalentTo(new byte[] { 0x31, 0x32, 0x33, 0x00, 0x31, 0x32, 0x33, 0x00, 0x00 });
        }

        [Fact]
        public void DeserializeCString()
        {
            var ms = new MemoryStream(new byte[] { 0x31, 0x32, 0x33, 0x00, 0x31, 0x32, 0x33, 0x00, 0x00 });
            var obj = BlubSerializer.Instance.Deserialize<CStringModel>(ms);
            obj.Value.Should().BeEquivalentTo("123");
            obj.Value2.Should().BeEquivalentTo("123");
        }

        [Fact]
        public void SerializeFixedArray()
        {
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, new FixedArrayModel(new byte[] { 1, 2, 3 }, new[] { 1, 2 }));
            ms.ToArray().Should().BeEquivalentTo(new byte[] { 1, 2, 3, 1, 0, 0, 0, 2, 0, 0, 0 });
        }

        [Fact]
        public void DeserializeFixedArray()
        {
            var ms = new MemoryStream(new byte[] { 1, 2, 3, 1, 0, 0, 0, 2, 0, 0, 0 });
            var obj = BlubSerializer.Instance.Deserialize<FixedArrayModel>(ms);
            obj.Value.Should().BeEquivalentTo(new byte[] { 1, 2, 3 });
            obj.Value2.Should().BeEquivalentTo(new[] { 1, 2 });
        }

        public enum TestEnum
        {
            A = 1
        }
    }
}
