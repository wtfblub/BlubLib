﻿using System;
using System.IO;
using BlubLib.Serialization.Tests.Models;
using BlubLib.Serialization.Tests.Serializers;
using FluentAssertions;
using Xunit;

namespace BlubLib.Serialization.Tests
{
    public class SerializerTests
    {
        [Fact]
        public void Serialize_WithSimpleType()
        {
            var expected = new byte[] { 3 };
            var foo = new SimpleModel { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithSimpleTypeAsObject()
        {
            var expected = new byte[] { 3 };
            var foo = new SimpleModel { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, (object)foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithInheritedType()
        {
            var expected = new byte[] { 3, 4 };
            var bar = new InheritedModel { A = 3, B = 4 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, bar);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithValueType()
        {
            var expected = new byte[] { 3 };
            var foo = new ModelStruct { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithValueTypeAsObject()
        {
            var expected = new byte[] { 3 };
            var foo = new ModelStruct { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, (object)foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithTypeWithoutAttributes()
        {
            var expected = new byte[] { 3 };
            var foo = new ModelWithoutAttributes { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithBinaryWriterAndSimpleType()
        {
            var expected = new byte[] { 3 };
            var foo = new SimpleModel { A = 3 };
            var ms = new MemoryStream();
            var writer = new BinaryWriter(ms);
            BlubSerializer.Instance.Serialize(writer, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithBinaryWriterAndSimpleTypeAsObject()
        {
            var expected = new byte[] { 3 };
            var foo = new SimpleModel { A = 3 };
            var ms = new MemoryStream();
            var writer = new BinaryWriter(ms);
            BlubSerializer.Instance.Serialize(writer, (object)foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithCustomSerializerOnAttribute()
        {
            var expected = new byte[] { 3 };
            var foo = new ModelWithCustomSerializerAttribute { A = 3 };
            var ms = new MemoryStream();
            var writer = new BinaryWriter(ms);
            BlubSerializer.Instance.Serialize(writer, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithCustomCompilerOnAttribute()
        {
            var expected = new byte[] { 3 };
            var foo = new ModelWithCustomCompilerAttribute { A = 3 };
            var ms = new MemoryStream();
            var writer = new BinaryWriter(ms);
            BlubSerializer.Instance.Serialize(writer, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithComplexType()
        {
            var expected = new byte[] { 3 };
            var foo = new ComplexModel { SimpleModel = new SimpleModel { A = 3 } };
            var ms = new MemoryStream();
            var writer = new BinaryWriter(ms);
            BlubSerializer.Instance.Serialize(writer, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithCustomSerializerOnProperty()
        {
            var expected = new byte[] { 3, 0, 0, 0 };
            var foo = new ModelWithCustomSerializerOnProperty { SimpleModel = new SimpleModel { A = 3 } };
            var ms = new MemoryStream();
            var writer = new BinaryWriter(ms);
            BlubSerializer.Instance.Serialize(writer, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithCustomCompilerOnProperty()
        {
            var expected = new byte[] { 3, 0, 0, 0 };
            var foo = new ModelWithCustomCompilerOnProperty { SimpleModel = new SimpleModel { A = 3 } };
            var ms = new MemoryStream();
            var writer = new BinaryWriter(ms);
            BlubSerializer.Instance.Serialize(writer, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Serialize_WithFields()
        {
            var expected = new byte[] { 1, 2 };
            var foo = new WithFieldsModel { A = 1, B = 2 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Deserialize_WithSimpleType()
        {
            var foo = new SimpleModel { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = BlubSerializer.Instance.Deserialize<SimpleModel>(ms);
            foo2.A.Should().Be(foo.A);
        }

        [Fact]
        public void Deserialize_WithSimpleTypeAsObject()
        {
            var foo = new SimpleModel { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = (SimpleModel)BlubSerializer.Instance.Deserialize(ms, typeof(SimpleModel));
            foo2.A.Should().Be(foo.A);
        }

        [Fact]
        public void Deserialize_WithInheritedType()
        {
            var bar = new InheritedModel { A = 3, B = 4 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, bar);
            ms.Position = 0;

            var bar2 = BlubSerializer.Instance.Deserialize<InheritedModel>(ms);
            bar2.A.Should().Be(bar.A);
            bar2.B.Should().Be(bar.B);
        }

        [Fact]
        public void Deserialize_WithValueType()
        {
            var foo = new ModelStruct { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = BlubSerializer.Instance.Deserialize<ModelStruct>(ms);
            foo2.A.Should().Be(foo.A);
        }

        [Fact]
        public void Deserialize_WithValueTypeAsObject()
        {
            var foo = new ModelStruct { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = (ModelStruct)BlubSerializer.Instance.Deserialize(ms, typeof(ModelStruct));
            foo2.A.Should().Be(foo.A);
        }

        [Fact]
        public void Deserialize_WithTypeWithoutAttributes()
        {
            var foo = new ModelWithoutAttributes { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = BlubSerializer.Instance.Deserialize<ModelWithoutAttributes>(ms);
            foo2.A.Should().Be(foo.A);
        }

        [Fact]
        public void Deserialize_WithBinaryReaderAndSimpleType()
        {
            var foo = new SimpleModel { A = 3 };
            var ms = new MemoryStream();
            var reader = new BinaryReader(ms);
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = BlubSerializer.Instance.Deserialize<SimpleModel>(reader);
            foo2.A.Should().Be(foo.A);
        }

        [Fact]
        public void Deserialize_WithBinaryReaderAndSimpleTypeAsObject()
        {
            var foo = new SimpleModel { A = 3 };
            var ms = new MemoryStream();
            var reader = new BinaryReader(ms);
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = (SimpleModel)BlubSerializer.Instance.Deserialize(reader, typeof(SimpleModel));
            foo2.A.Should().Be(foo.A);
        }

        [Fact]
        public void Deserialize_WithCustomSerializerOnAttribute()
        {
            var foo = new ModelWithCustomSerializerAttribute { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = BlubSerializer.Instance.Deserialize<ModelWithCustomSerializerAttribute>(ms);
            foo2.A.Should().Be(foo.A);
        }

        [Fact]
        public void Deserialize_WithCustomCompilerOnAttribute()
        {
            var foo = new ModelWithCustomCompilerAttribute { A = 3 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = BlubSerializer.Instance.Deserialize<ModelWithCustomCompilerAttribute>(ms);
            foo2.A.Should().Be(foo.A);
        }

        [Fact]
        public void Deserialize_WithComplexType()
        {
            var foo = new ComplexModel { SimpleModel = new SimpleModel { A = 3 } };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = BlubSerializer.Instance.Deserialize<ComplexModel>(ms);
            foo2.SimpleModel.A.Should().Be(foo.SimpleModel.A);
        }

        [Fact]
        public void Deserialize_WithWithCustomSerializerOnProperty()
        {
            var foo = new ModelWithCustomSerializerOnProperty { SimpleModel = new SimpleModel { A = 3 } };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = BlubSerializer.Instance.Deserialize<ModelWithCustomSerializerOnProperty>(ms);
            foo2.SimpleModel.A.Should().Be(foo.SimpleModel.A);
        }

        [Fact]
        public void Deserialize_WithWithCustomCompilerOnProperty()
        {
            var foo = new ModelWithCustomCompilerOnProperty { SimpleModel = new SimpleModel { A = 3 } };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = BlubSerializer.Instance.Deserialize<ModelWithCustomCompilerOnProperty>(ms);
            foo2.SimpleModel.A.Should().Be(foo.SimpleModel.A);
        }

        [Fact]
        public void Deserialize_WithFields()
        {
            var foo = new WithFieldsModel { A = 1, B = 2 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.Position = 0;

            var foo2 = BlubSerializer.Instance.Deserialize<WithFieldsModel>(ms);
            foo2.A.Should().Be(foo.A);
            foo2.B.Should().Be(foo.B);
        }

        [Fact]
        public void AddSerializer_ShouldThrowArgumentException_WhenSerializerAlreadyAdded()
        {
            var serializer = new ModelWithCustomSerializerAttributeSerializer();
            BlubSerializer.Instance.AddSerializer(serializer);
            Assert.Throws<ArgumentException>(() => BlubSerializer.Instance.AddSerializer(serializer));
        }

        [Fact]
        public void AddSerializer_ShouldThrowArgumentException_WhenCompilerAlreadyAdded()
        {
            var serializer = new ModelWithCustomCompilerAttributeCompiler();
            BlubSerializer.Instance.AddSerializer(serializer);
            Assert.Throws<ArgumentException>(() => BlubSerializer.Instance.AddSerializer(serializer));
        }

        [Fact]
        public void MemberOrderIfProvidedHasToBeOnAllMembers()
        {
            Assert.Throws<SerializerException>(() => BlubSerializer.Instance.GetSerializer<OneMemberOrderModel>())
                .Message.Should().BeEquivalentTo($"Member '{typeof(OneMemberOrderModel).FullName}.{nameof(MemberOrderNotUniqueModel.B)}' has not a unique order assigned");
        }

        [Fact]
        public void MemberOrderHasToBeUnique()
        {
            Assert.Throws<SerializerException>(() => BlubSerializer.Instance.GetSerializer<MemberOrderNotUniqueModel>())
                .Message.Should().BeEquivalentTo($"Member '{typeof(MemberOrderNotUniqueModel).FullName}.{nameof(MemberOrderNotUniqueModel.B)}' has not a unique order assigned");
        }

        [Fact]
        public void OnlyIncludeMembersWithBlubMemberIfMarkedWithBlubContract()
        {
            var expected = new byte[] { 1, 3 };
            var foo = new WithContractModel { A = 1, B = 2, C = 3, D = 4 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void IncludeAllMembersIfNotMarkedWithBlubContract()
        {
            var expected = new byte[] { 1, 3 };
            var foo = new WithoutContractModel { A = 1, B = 2, C = 3, D = 4 };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, foo);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void BeforeSerialize()
        {
            HooksModel.BeforeDeserializeFunc = null;
            HooksModel.BeforeSerializeFunc = (_, __, memberName) => memberName == nameof(HooksModel.A);
            HooksStructModel.BeforeDeserializeFunc = null;
            HooksStructModel.BeforeSerializeFunc = (_, __, memberName) => memberName == nameof(HooksStructModel.A);

            var expected = new byte[] { 2 };
            var model = new HooksModel
            {
                A = 1,
                B = 2
            };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, model);
            ms.ToArray().Should().BeEquivalentTo(expected);

            var structModel = new HooksStructModel
            {
                A = 1,
                B = 2
            };
            ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, structModel);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void BeforeDeserialize()
        {
            HooksModel.BeforeSerializeFunc = null;
            HooksModel.BeforeDeserializeFunc = (_, __, memberName) => memberName == nameof(HooksModel.A);
            HooksStructModel.BeforeSerializeFunc = null;
            HooksStructModel.BeforeDeserializeFunc = (_, __, memberName) => memberName == nameof(HooksStructModel.A);

            var ms = new MemoryStream(new byte[] { 2 });
            var model = BlubSerializer.Instance.Deserialize<HooksModel>(ms);
            model.A.Should().Be(default(byte));
            model.B.Should().Be(2);

            ms = new MemoryStream(new byte[] { 2 });
            var structModel = BlubSerializer.Instance.Deserialize<HooksStructModel>(ms);
            structModel.A.Should().Be(default(byte));
            structModel.B.Should().Be(2);
        }

        [Fact]
        public void AfterSerialize()
        {
            var numCalls = 0;

            HooksModel.BeforeSerializeFunc = null;
            HooksModel.BeforeDeserializeFunc = null;
            HooksModel.AfterSerializeFunc = (_, __, memberName) => ++numCalls;
            HooksStructModel.BeforeSerializeFunc = null;
            HooksStructModel.BeforeDeserializeFunc = null;
            HooksStructModel.AfterSerializeFunc = (_, __, memberName) => ++numCalls;

            var expected = new byte[] { 1, 2 };
            var expectedCallCount = 2;

            var model = new HooksModel
            {
                A = 1,
                B = 2
            };
            var ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, model);
            ms.ToArray().Should().BeEquivalentTo(expected);
            numCalls.Should().Be(expectedCallCount);
            numCalls = 0;

            var structModel = new HooksStructModel
            {
                A = 1,
                B = 2
            };
            ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, structModel);
            ms.ToArray().Should().BeEquivalentTo(expected);
            numCalls.Should().Be(expectedCallCount);
            numCalls = 0;

            HooksModel.BeforeSerializeFunc = (_, __, memberName) => memberName == nameof(HooksModel.A);
            HooksStructModel.BeforeSerializeFunc = (_, __, memberName) => memberName == nameof(HooksStructModel.A);

            expected = new byte[] { 2 };
            expectedCallCount = 1;

            ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, model);
            ms.ToArray().Should().BeEquivalentTo(expected);
            numCalls.Should().Be(expectedCallCount);
            numCalls = 0;

            ms = new MemoryStream();
            BlubSerializer.Instance.Serialize(ms, structModel);
            ms.ToArray().Should().BeEquivalentTo(expected);
            numCalls.Should().Be(expectedCallCount);
        }

        [Fact]
        public void AfterDeserialize()
        {
            var numCalls = 0;

            HooksModel.BeforeSerializeFunc = null;
            HooksModel.BeforeDeserializeFunc = null;
            HooksModel.AfterDeserializeFunc = (_, __, memberName) => ++numCalls;
            HooksStructModel.BeforeSerializeFunc = null;
            HooksStructModel.BeforeDeserializeFunc = null;
            HooksStructModel.AfterDeserializeFunc = (_, __, memberName) => ++numCalls;

            var ms = new MemoryStream(new byte[] { 1, 2 });
            var model = BlubSerializer.Instance.Deserialize<HooksModel>(ms);
            model.A.Should().Be(1);
            model.B.Should().Be(2);
            numCalls.Should().Be(2);
            numCalls = 0;

            ms = new MemoryStream(new byte[] { 1, 2 });
            var structModel = BlubSerializer.Instance.Deserialize<HooksStructModel>(ms);
            model.A.Should().Be(1);
            model.B.Should().Be(2);
            numCalls.Should().Be(2);
            numCalls = 0;

            HooksModel.BeforeDeserializeFunc = (_, __, memberName) => memberName == nameof(HooksModel.A);
            HooksStructModel.BeforeDeserializeFunc = (_, __, memberName) => memberName == nameof(HooksStructModel.A);
            ms = new MemoryStream(new byte[] { 2 });
            model = BlubSerializer.Instance.Deserialize<HooksModel>(ms);
            model.A.Should().Be(default(byte));
            model.B.Should().Be(2);
            numCalls.Should().Be(1);
            numCalls = 0;

            ms = new MemoryStream(new byte[] { 2 });
            structModel = BlubSerializer.Instance.Deserialize<HooksStructModel>(ms);
            structModel.A.Should().Be(default(byte));
            structModel.B.Should().Be(2);
            numCalls.Should().Be(1);
        }

        [Fact]
        public void BeforeSerialize_ShouldThrowOnInvalidSignature()
        {
            void Ensure<TModel>()
            {
                var ex = Assert.Throws<SerializerException>(() =>
                    BlubSerializer.Instance.GetSerializer<TModel>());
                ex.Message.Should().BeEquivalentTo(
                    $"Method '{typeof(TModel).FullName}.Hook' marked with {nameof(BlubBeforeSerializeAttribute)} requires the signature bool ({nameof(BlubSerializer)}, BinaryWriter, string)"
                );
            }

            Ensure<InvalidBeforeSerializeModel.InvalidParameter1Model>();
            Ensure<InvalidBeforeSerializeModel.InvalidParameter2Model>();
            Ensure<InvalidBeforeSerializeModel.InvalidParameter3Model>();
            Ensure<InvalidBeforeSerializeModel.InvalidParameter4Model>();
            Ensure<InvalidBeforeSerializeModel.InvalidReturnModel>();
        }

        [Fact]
        public void BeforeDeserialize_ShouldThrowOnInvalidSignature()
        {
            void Ensure<TModel>()
            {
                var ex = Assert.Throws<SerializerException>(() =>
                    BlubSerializer.Instance.GetSerializer<TModel>());
                ex.Message.Should().BeEquivalentTo(
                    $"Method '{typeof(TModel).FullName}.Hook' marked with {nameof(BlubBeforeDeserializeAttribute)} requires the signature bool ({nameof(BlubSerializer)}, BinaryReader, string)"
                );
            }

            Ensure<InvalidBeforeDeserializeModel.InvalidParameter1Model>();
            Ensure<InvalidBeforeDeserializeModel.InvalidParameter2Model>();
            Ensure<InvalidBeforeDeserializeModel.InvalidParameter3Model>();
            Ensure<InvalidBeforeDeserializeModel.InvalidParameter4Model>();
            Ensure<InvalidBeforeDeserializeModel.InvalidReturnModel>();
        }

        [Fact]
        public void AfterSerialize_ShouldThrowOnInvalidSignature()
        {
            void Ensure<TModel>()
            {
                var ex = Assert.Throws<SerializerException>(() =>
                    BlubSerializer.Instance.GetSerializer<TModel>());
                ex.Message.Should().BeEquivalentTo(
                    $"Method '{typeof(TModel).FullName}.Hook' marked with {nameof(BlubAfterSerializeAttribute)} requires the signature void ({nameof(BlubSerializer)}, BinaryWriter, string)"
                );
            }

            Ensure<InvalidAfterSerializeModel.InvalidParameter1Model>();
            Ensure<InvalidAfterSerializeModel.InvalidParameter2Model>();
            Ensure<InvalidAfterSerializeModel.InvalidParameter3Model>();
            Ensure<InvalidAfterSerializeModel.InvalidParameter4Model>();
            Ensure<InvalidAfterSerializeModel.InvalidReturnModel>();
        }

        [Fact]
        public void AfterDeserialize_ShouldThrowOnInvalidSignature()
        {
            void Ensure<TModel>()
            {
                var ex = Assert.Throws<SerializerException>(() =>
                    BlubSerializer.Instance.GetSerializer<TModel>());
                ex.Message.Should().BeEquivalentTo(
                    $"Method '{typeof(TModel).FullName}.Hook' marked with {nameof(BlubAfterDeserializeAttribute)} requires the signature void ({nameof(BlubSerializer)}, BinaryReader, string)"
                );
            }

            Ensure<InvalidAfterDeserializeModel.InvalidParameter1Model>();
            Ensure<InvalidAfterDeserializeModel.InvalidParameter2Model>();
            Ensure<InvalidAfterDeserializeModel.InvalidParameter3Model>();
            Ensure<InvalidAfterDeserializeModel.InvalidParameter4Model>();
            Ensure<InvalidAfterDeserializeModel.InvalidReturnModel>();
        }

        [Fact]
        public void CanOverwritePrimitiveWithSerializer()
        {
            var serializer = new BlubSerializer();
            serializer.AddSerializer(new Int32AsByteSerializer());

            var expected = new byte[] { 1 };
            var ms = new MemoryStream();
            serializer.Serialize(ms, 1);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void CanOverwritePrimitiveWithCompiler()
        {
            var serializer = new BlubSerializer();
            serializer.AddSerializer(new Int32AsByteCompiler());

            var expected = new byte[] { 1 };
            var ms = new MemoryStream();
            serializer.Serialize(ms, 1);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void CanOverwriteEnumSerializer()
        {
            var serializer = new BlubSerializer();
            serializer.AddSerializer(new EnumAsByteSerializer());

            var expected = new [] { (byte)SerializersTests.TestEnum.A };
            var ms = new MemoryStream();
            serializer.Serialize(ms, SerializersTests.TestEnum.A);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ArraySerializer_DoesNotThrow()
        {
            var serializer = new BlubSerializer();
            serializer.AddSerializer(new ByteArraySerializer());

            var expected = new byte[] { 0x03, 0x00, 0x00, 0x00, 0x01, 0x02, 0x03 };
            var data = new byte[] { 1, 2, 3 };
            var ms = new MemoryStream();
            serializer.Serialize(ms, data);
            ms.ToArray().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ThrowOnCanHandleFalse()
        {
            var ex = Assert.Throws<SerializerException>(() =>
                BlubSerializer.Instance.GetSerializer<CantHandleModel>());
            ex.Message.Should().BeEquivalentTo(
                $"Serializer '{typeof(CantHandleSerializer).FullName}' can not handle '{typeof(CantHandleModel).FullName}'"
            );

            ex = Assert.Throws<SerializerException>(() =>
                BlubSerializer.Instance.GetSerializer<CantHandleOnMemberModel>());
            ex.Message.Should().BeEquivalentTo(
                $"Serializer '{typeof(CantHandleSerializer).FullName}' on member '{typeof(CantHandleOnMemberModel).FullName}.{nameof(CantHandleOnMemberModel.A)}' can not handle '{typeof(int).FullName}'"
            );
        }
    }
}
