using System;
using System.IO;

namespace BlubLib.Serialization.Tests.Serializers
{
    public class ByteArraySerializer : ISerializer<byte[]>
    {
        public bool CanHandle(Type type)
        {
            return type.IsArray && type.GetElementType() == typeof(byte);
        }

        public void Serialize(BlubSerializer blubSerializer, BinaryWriter writer, byte[] value)
        {
            writer.Write(value.Length);
            writer.Write(value);
        }

        public byte[] Deserialize(BlubSerializer blubSerializer, BinaryReader reader)
        {
            return reader.ReadBytes(reader.ReadInt32());
        }
    }
}
