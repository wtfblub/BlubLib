using System;
using System.IO;

namespace BlubLib.Serialization.Tests.Serializers
{
    public class CantHandleSerializer : ISerializer<int>
    {
        public bool CanHandle(Type type)
        {
            return false;
        }

        public void Serialize(BlubSerializer blubSerializer, BinaryWriter writer, int value)
        {
            throw new NotImplementedException();
        }

        public int Deserialize(BlubSerializer blubSerializer, BinaryReader reader)
        {
            throw new NotImplementedException();
        }
    }
}
