using System;
using Sigil;

namespace BlubLib.Serialization.Tests.Serializers
{
    public class EnumAsByteSerializer : ISerializerCompiler
    {
        public bool CanHandle(Type type)
        {
            return type.IsEnum;
        }

        public void EmitDeserialize(CompilerContext ctx, Local value)
        {
            var underlyingType = value.LocalType.GetEnumUnderlyingType();
            var typeToUse = typeof(byte);

            using (var tmp = ctx.Emit.DeclareLocal(typeToUse))
            {
                ctx.EmitDeserialize(tmp);
                ctx.Emit.LoadLocal(tmp);
                if (underlyingType != typeToUse)
                    ctx.Emit.Convert(underlyingType);

                ctx.Emit.StoreLocal(value);
            }
        }

        public void EmitSerialize(CompilerContext ctx, Local value)
        {
            var underlyingType = value.LocalType.GetEnumUnderlyingType();
            var typeToUse = typeof(byte);

            using (var tmp = ctx.Emit.DeclareLocal(typeToUse))
            {
                ctx.Emit.LoadLocal(value);
                if (underlyingType != typeToUse)
                    ctx.Emit.Convert(underlyingType);

                ctx.Emit.StoreLocal(tmp);
                ctx.EmitSerialize(tmp);
            }
        }
    }
}
